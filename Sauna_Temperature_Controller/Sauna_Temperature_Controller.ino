#include <LiquidCrystal.h>
#include <Wire.h>
#include "Adafruit_MCP9808.h"

const int analogInPin = A0;
const int RelayPin = 6;

const int SampleTime = 500;

unsigned long previousMillis = 0;

const int Hysteresis1 = 0;

const int Hysteresis2 = 2;

enum HeaterStates {On, Off};

enum HeaterStates HeaterState = Off;

String Message = String("Off");

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(7, 8, 9, 10, 11, 12);

Adafruit_MCP9808 tempsensor0 = Adafruit_MCP9808();
Adafruit_MCP9808 tempsensor1 = Adafruit_MCP9808();


void setup() {
  
  pinMode(RelayPin, OUTPUT);

  Serial.begin(115200);

  if (!tempsensor0.begin(0x18)) {
    Serial.println("Couldn't find MCP9808!");
    while (1);
  }

  if (!tempsensor1.begin(0x19)) {
    Serial.println("Couldn't find MCP9808!");
    while (1);
  }
  
  lcd.begin(16, 2);

}

void loop() {

  unsigned long currentMillis = millis();
  int SollTemperatur = map(analogRead(analogInPin), 0, 1023, 40, 100);

  if (currentMillis - previousMillis >= SampleTime) {
    //////////////////////////////////////////////////////////////////////////
    // Read
    //////////////////////////////////////////////////////////////////////////
    float IstTemperatur0 = tempsensor0.readTempC();
    float IstTemperatur1 = tempsensor1.readTempC();
    //////////////////////////////////////////////////////////////////////////
    // Compute
    //////////////////////////////////////////////////////////////////////////
    int MeanTemperatur = (int)(((IstTemperatur0 + IstTemperatur1) / 2.0) + 0.5);
    //////////////////////////////////////////////////////////////////////////
    // Control
    //////////////////////////////////////////////////////////////////////////
    if (HeaterState == Off && MeanTemperatur <= (SollTemperatur - Hysteresis2)) {
      Message = String(" On");
      HeaterState = On;
      digitalWrite(RelayPin, HIGH);
    }

    // Transition from On to Off
    if (HeaterState == On && MeanTemperatur >= (SollTemperatur - Hysteresis1)) {
      Message = String("Off");
      HeaterState = Off;
      digitalWrite(RelayPin, LOW);
    }
    //////////////////////////////////////////////////////////////////////////
    // Visualization on LCD
    //////////////////////////////////////////////////////////////////////////

    int IstTemperatur0Vis = (int)(IstTemperatur0 + 0.5);
    int IstTemperatur1Vis = (int)(IstTemperatur1 + 0.5);

    // Ist Sensor 0
    lcd.setCursor(1, 0);
    lcd.print("Ist:");

    if (IstTemperatur0 < 100) {
      lcd.setCursor(5, 0);
      lcd.print(" ");
      lcd.setCursor(6, 0);
      lcd.print(IstTemperatur0Vis);
    }
    else {
      lcd.setCursor(5, 0);
      lcd.print(IstTemperatur0Vis);
    }

    // Ist Sensor 1
    lcd.setCursor(9, 0);
    lcd.print("Ist:");

    if (IstTemperatur1 < 100) {
      lcd.setCursor(13, 0);
      lcd.print(" ");
      lcd.setCursor(14, 0);
      lcd.print(IstTemperatur1Vis);
    }
    else {
      lcd.setCursor(13, 0);
      lcd.print(IstTemperatur1Vis);
    }

    // Soll
    lcd.setCursor(0, 1);
    lcd.print("Soll:");
    if (SollTemperatur < 100) {
      lcd.setCursor(5, 1);
      lcd.print(" ");
      lcd.setCursor(6, 1);
      lcd.print(SollTemperatur);
    }
    else {
      lcd.setCursor(5, 1);
      lcd.print(SollTemperatur);
    }
    
    // Heating
    lcd.setCursor(11, 1);
    lcd.print("H:");
    lcd.print(Message);

    previousMillis = currentMillis;
    
  }

}

